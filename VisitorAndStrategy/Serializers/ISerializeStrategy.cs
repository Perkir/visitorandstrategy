using VisitorAndStrategy.Visitor;

namespace VisitorAndStrategy.Serializers
{
    public interface ISerializeStrategy
    {
        string SerializeObj<T>(T obj, IVisitor visitor);
    }
}