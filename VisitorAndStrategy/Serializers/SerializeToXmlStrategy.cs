using System;
using System.IO;
using System.Xml.Serialization;
using VisitorAndStrategy.Visitor;

namespace VisitorAndStrategy.Serializers
{
    public class SerializeToXmlStrategy: ISerializeStrategy
    {
        public string SerializeObj<T>(T obj, IVisitor visitor)
        {
            return visitor.visit(this, obj);
        }
    }
}