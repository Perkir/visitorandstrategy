using VisitorAndStrategy.Visitor;

namespace VisitorAndStrategy.Serializers
{
    public class SerializeToJsonStrategy: ISerializeStrategy
    {
        public string SerializeObj<T>(T obj, IVisitor visitor)
        {
            return visitor.visit(this, obj);
        }
    }
}