using System;

namespace VisitorAndStrategy.Figures
{
    [Serializable]
    public class Circle
    {
        public int Radius { get; set; }
        
        public Circle(int radius)
        {
            this.Radius = radius;
        }
        public Circle(){}
        
        public override string ToString()
        {
            return $"radius:{Radius};";
        }
    }
}