using System;
using VisitorAndStrategy.Visitor;

namespace VisitorAndStrategy.Figures
{
    [Serializable]
    public class Square
    {
        public int LeftSide { get; set; }
        public int RightSide { get; set; }
        public int BottomSide { get; set; }
        public int UpperSide { get; set; }

        public Square(int upperSide, int leftSide, int rightSide, int bottomSide)
        {
            this.BottomSide = bottomSide;
            this.UpperSide = upperSide;
            this.LeftSide = leftSide;
            this.RightSide = rightSide;
        }

        public Square()
        {
        }

        public override string ToString()
        {
            return $"LeftSide:{LeftSide};RightSide:{RightSide};BottomSide:{BottomSide};UpperSide:{UpperSide};";
        }
    }
}