using System;

namespace VisitorAndStrategy.Figures
{
    [Serializable]
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point()
        {
        }
        public override string ToString()
        {
            return $"x:{X}; y:{Y}";
        }

    }
}