using System;
using VisitorAndStrategy.Figures;
using VisitorAndStrategy.Serializers;
using VisitorAndStrategy.Visitor;

namespace VisitorAndStrategy
{
    public class Context
    {
        public void DoPoint() 
        {
            var point = new Point {X = 10, Y = 20};
            ISerializeStrategy jsonStrategy = new SerializeToJsonStrategy();
            ISerializeStrategy xmlStrategy = new SerializeToXmlStrategy();
            
            var pointVisit = new PointVisitor();
            string json = jsonStrategy.SerializeObj(point, pointVisit);
            string xml = xmlStrategy.SerializeObj(point, pointVisit);
            Console.WriteLine(json);
            Console.WriteLine(xml);
        }

        public void DoCircle()
        {
            var circle = new Circle {Radius = 30};
            ISerializeStrategy jsonStrategy = new SerializeToJsonStrategy();
            ISerializeStrategy xmlStrategy = new SerializeToXmlStrategy();
            
            var circletVisit = new CircleVisitor();
            string json = jsonStrategy.SerializeObj(circle, circletVisit);
            string xml = xmlStrategy.SerializeObj(circle, circletVisit);
            Console.WriteLine(json);
            Console.WriteLine(xml);
        }

        public void DoSquare()
        {
            var square = new Square {LeftSide = 30, BottomSide = 10, RightSide = 15, UpperSide = 40};
            ISerializeStrategy jsonStrategy = new SerializeToJsonStrategy();
            ISerializeStrategy xmlStrategy = new SerializeToXmlStrategy();
            
            var squaretVisit = new SquareVisitor();
            string json = jsonStrategy.SerializeObj(square, squaretVisit);
            string xml = xmlStrategy.SerializeObj(square, squaretVisit);
            Console.WriteLine(json);
            Console.WriteLine(xml);
        }
    }
    
}