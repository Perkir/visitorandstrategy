using VisitorAndStrategy.Serializers;

namespace VisitorAndStrategy.Visitor
{
    public class CircleVisitor: IVisitor
    {
        public string visit<T>(SerializeToJsonStrategy strategy, T obj)
        {
            return $"{{\"circle\": {{{obj}}} }}";
        }

        public string visit<T>(SerializeToXmlStrategy strategy,  T obj)
        {
            return $"<circle>{obj}</circle>";
        }
    }
}