using VisitorAndStrategy.Figures;
using VisitorAndStrategy.Serializers;

namespace VisitorAndStrategy.Visitor
{
    public interface IVisitor
    {
       string visit<T>(SerializeToJsonStrategy strategy, T obj) ;
        string visit<T>(SerializeToXmlStrategy strategy, T obj) ;


       //  string doForSquare(Square a);


       //  string doForCircle(Circle circle);
        

       //  string doForPoint(Point ss);
        

    }
}