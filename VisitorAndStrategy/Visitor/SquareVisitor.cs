using VisitorAndStrategy.Serializers;

namespace VisitorAndStrategy.Visitor
{
    public class SquareVisitor: IVisitor
    {
        public string visit<T>(SerializeToJsonStrategy strategy, T obj)
        {
            return $"{{\"square\": {{{obj}}} }}";
        }

        public string visit<T>(SerializeToXmlStrategy strategy, T obj)
        {
            return $"<square>{obj}</square>";
        }
    }
}