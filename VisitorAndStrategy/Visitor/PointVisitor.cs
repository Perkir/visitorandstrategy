using VisitorAndStrategy.Serializers;

namespace VisitorAndStrategy.Visitor
{
    public class PointVisitor: IVisitor
    {
        public string visit<T>(SerializeToJsonStrategy strategy, T obj)
        {
            return $"{{\"point\": {{{obj}}} }}";
        }

        public string visit<T>(SerializeToXmlStrategy strategy, T obj)
        {
            return $"<point>{obj}</point>";
        }
    }
}