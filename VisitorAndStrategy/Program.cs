﻿
namespace VisitorAndStrategy
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = new Context();
            
            c.DoPoint();
            c.DoCircle();
            c.DoSquare();
        }
    }
}